program AC_snl  ("no=1")

option +r;

double t_in;
assign t_in to "advancedAC:t_in.VAL";
monitor t_in;

double t_out;
assign t_out to "advancedAC:t_out.VAL";
monitor t_out;

double delay;
assign delay to "advancedAC:delayT_out.VAL";
monitor delay;

double count;
assign count to "advancedAC:counter.VAL";
monitor count;

ss outisdeAdd {
    state addOutTemp {
        when(delay(delay)) {
            t_in += .1 * (t_out - t_in);
            pvPut(t_in, SYNC);
        }
        state calcT_out
    }

    state calcT_out {
        when() {
            if (count >= 24)
                count = 0;
            else if (count >= 12) {
                count++;
                t_out -= 1.5;
            } else {
                count++;
                t_out += 1.5;
            }
            pvPut(count);
            pvPut(t_out);
        }
        state addOutTemp
    }
}
