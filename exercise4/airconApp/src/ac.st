program ac ("no=1")

option +r;

double t_in;
assign t_in to "advancedAC:t_in.VAL";
monitor t_in;

char AC_on;
assign AC_on to "advancedAC:AC{no}_on.VAL";
monitor AC_on;

char t_ref;
assign t_ref to "advancedAC:t_ref.VAL";
monitor t_ref;

char P_max;
assign P_max to "advancedAC:P_max{no}.VAL";
monitor P_max;

char P_min;
assign P_min to "advancedAC:P_min{no}.VAL";
monitor P_min;

double AC;
assign AC to "advancedAC:AC{no}.VAL";
monitor AC;

double Power;
assign Power to "advancedAC:Power{no}.VAL";
monitor Power;

short cooling;
assign cooling to "advancedAC:cooling{no}.VAL";
monitor cooling;

short heating;
assign heating to "advancedAC:heating{no}.VAL";
monitor heating;

char deadband;
assign deadband to "advancedAC:deadband{no}.VAL";
monitor deadband;

double delay2;
assign delay2 to "advancedAC:delayAC.VAL";
monitor delay2;



ss ac {
    state cooling {
        when(delay(delay2)) {
            if (t_in - t_ref > deadband || cooling && t_ref < t_in) {
                cooling = -1;
            } else {
                cooling = 0;
            }
            pvPut(cooling);
        }
        state heating
    }
    state heating {
        when() {
            if (t_ref - t_in > deadband || (heating && t_ref > t_in))
                heating = 1;

            else {
                heating = 0;
            }
            pvPut(heating);
        }
        state ACcalcPwr
    }

    state ACcalcPwr {
        when() {
            if (t_in > t_ref)
                Power = P_max / (6 - (t_in - t_ref));

            else if (t_in < t_ref)
                Power = P_max / (6 + (t_in - t_ref));
            if (Power > P_max)
                Power = P_max;
            else if (Power < P_min)
                Power = P_min;

            pvPut(Power);
        }
        state AC
    }
    state AC {
        when() {
            if (heating || cooling) {
                AC = AC_on * (heating + cooling) * Power;
            } else {
                AC = 0;
            }
            t_in += AC;
            pvPut(t_in, SYNC);
            pvPut(AC);
        }
        state cooling
    }
}
