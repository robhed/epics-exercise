#!../../bin/linux-x86_64/aircon

## You may have to change aircon to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/aircon.dbd"
aircon_registerRecordDeviceDriver pdbbase

epicsEnvSet("DEFAULT_AMPLITUDE" "7")
## Load record instances
#dbLoadRecords("db/outdoor.db","theta=2/24, Amp=7, task=outdoor")
#dbLoadRecords("db/indoor.db", "task=indoor, theta=.5, Amp=7, R=.2")
#dbLoadTemplate("db/ac.substitutions", "task=constCool, no=1")
#dbLoadRecords("db/constCoolingTemp.db", "task=constCool, theta=.5, Amp=7, R=.2") 
dbLoadRecords("db/advancedACTemp.db", "task=advancedAC, theta=.5, Amp=$(DEFAULT_AMPLITUDE), R=.2")
dbLoadRecords("db/advancedAC.db", "task=advancedAC, no=1")
dbLoadRecords("db/advancedAC.db", "task=advancedAC, no=2")

cd ${TOP}/iocBoot/${IOC}
iocInit

seq ac, "no=1"
seq &ac, "no=2"

seq AC_snl

## Start any sequence programs
#seq sncxxx,"user=robhedVMHost"
