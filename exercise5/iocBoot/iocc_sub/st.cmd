#!../../bin/linux-x86_64/c_sub

## You may have to change c_sub to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/c_sub.dbd"
c_sub_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/dataBase.db","user=robhedVMHost")
dbLoadRecords("db/waveRec.db", "no=1")
dbLoadRecords("db/waveRec.db", "no=2")
dbLoadRecords("db/waveRec.db", "no=Out")

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncxxx,"user=robhedVMHost"
