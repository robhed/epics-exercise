#include <stdio.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <waveformRecord.h>

static int cTest(aSubRecord *precord){

  double *weight1, *weight2, *pos1, *pos2, *out;

  out = (double *)precord->vala;
  weight1 = (double *)precord->a;
  weight2 = (double *)precord->b;
  pos1 = (double *)precord->c;
  pos2 = (double *)precord->d;

  int i =0;
  for(;i<3;++i){
    if(*weight1 + *weight2 == 0) *out = 0;
    else *(out) = (*weight1*(*pos1)+*weight2*(*pos2))/(*weight1+*weight2);
    ++out;
    ++pos1;
    ++pos2;
  }
  // int i=0;
  // for(;i<3;++i) out[i] = (*weight1*pos1[i]+*weight2*pos2[i])/(*weight1+*weight2);
  return 0;
}


epicsRegisterFunction(cTest);
