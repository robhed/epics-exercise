#!../../bin/linux-x86_64/ex2

## You may have to change ex2 to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/ex2.dbd"
ex2_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadTemplate "db/userHost.substitutions"
dbLoadRecords "db/dbSubExample.db", "user=robhedVMHost"
dbLoadRecords "db/dbCount.db", "user=robhedVMHost", "amp=10", "ph=3","theta=2"

## Set this to see messages from mySub
#var mySubDebug 1

## Run this to trace the stages of iocInit
#traceIocInit

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncExample, "user=robhedVMHost"
