#!../../bin/linux-x86_64/devsup

## You may have to change devsup to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/devsup.dbd"
devsup_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/database.db","user=robhedVMHost")
dbLoadRecords("db/multipleSupport.db")

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncxxx,"user=robhedVMHost"
