#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "alarm.h"
#include "dbDefs.h"
#include "dbAccess.h"
#include "recGbl.h"
#include "devSup.h"
#include "aiRecord.h"
#include "boRecord.h"
#include "epicsExport.h"

static long init_record(aiRecord *precord);
static long read_ai(aiRecord *precord);

char *inputChoise="";

struct {
  long number;
  DEVSUPFUN report;
  DEVSUPFUN init;
  DEVSUPFUN init_record;
  DEVSUPFUN get_ioint_info;
  DEVSUPFUN read_ai;
  DEVSUPFUN special_linconv;
} aiStruct = {
  6,
  NULL, 
  NULL, 
  init_record,
  NULL, 
  read_ai,
  NULL
};


epicsExportAddress(dset, aiStruct);


static long init_record(aiRecord *precord){
  return 0;
}


static long read_ai(aiRecord *precord){
  time_t t;
  struct tm *tm;

  char *inpParameter = precord->inp.value.constantStr;
  double rnd = ((double)rand())/((double)RAND_MAX);
  precord->udf = FALSE;
  if(!strcmp(inpParameter, "time") || !strcmp(inpParameter, "random")){
    if(strcmp(inpParameter, "time")){
      precord->val =rnd;
    }
    else{   
      time(&t);
      tm = localtime(&t);
      precord->val = tm->tm_sec;
    }
  }
  else{
    if(!strcmp(inputChoise,"random")) precord->val =rnd;
    else {
      time(&t);
      tm = localtime(&t);
      precord->val = tm->tm_sec;

    }
  }
  return 2;
}

static long write_bo(boRecord *precord);

struct {
  long number;
  DEVSUPFUN report;
  DEVSUPFUN init;
  DEVSUPFUN init_record;
  DEVSUPFUN get_ioint_info;
  DEVSUPFUN write_bo;
} boStruct = {
  5,
  NULL, 
  NULL,
  NULL,
  NULL, 
  write_bo
};

epicsExportAddress(dset, boStruct);

static long write_bo(boRecord *precord){
  inputChoise=precord->onam;
  return 0;
}
